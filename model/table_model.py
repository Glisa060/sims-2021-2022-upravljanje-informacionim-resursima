from PySide2.QtCore import Qt, QAbstractTableModel, QModelIndex
from time import time


class MyTableModel(QAbstractTableModel):
    def __init__(self, parent, mylist, header, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        self.mylist = list(mylist)
        self.header = list(header)

    def rowCount(self, parent):
        return len(self.mylist[0])

    def columnCount(self, parent):
        return len(self.header)

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role != Qt.DisplayRole:
            return None
        return self.mylist[index.column()][index.row()]

    def setData(self, index, value):
        self.mylist[index.row()][index.column()] = value
        return True

    def flags(self, index):
        return Qt.ItemIsEnabled | Qt.ItemIsEditable | Qt.ItemIsSelectable

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header[col]
        return None

    def flags(self, index):
        return Qt.ItemIsEnabled | Qt.ItemIsEditable | Qt.ItemIsSelectable

    def insertRows(self, position, rows, item, parent=QModelIndex()):
        self.beginInsertRows(QModelIndex(), len(self.mylist), len(self.mylist) + 1)
        self.mylist.append(item)  # Item must be an array
        self.endInsertRows()
        return True
