from PySide2.QtWidgets import QDockWidget, QFileSystemModel, QTreeView, QVBoxLayout
from PySide2.QtCore import QDir


class FileDockWidget(QDockWidget):
    def __init__(self, title="", parent=None) -> None:
        super().__init__(title, parent)
        self.path1 = "C:/Users/Milan/Documents/sims-2021-2022-upravljanje-informacionim-resursima/data/users.csv"
        self.path = r"C:/Users/Milan/Documents/sims-2021-2022-upravljanje-informacionim-resursima/data"
        self.populate(self.path)
        # self.tree_view.clicked.connect(self.onClicked)

    def populate(self, path):
        path = r"C:/Users/Milan/Documents/sims-2021-2022-upravljanje-informacionim-resursima/data"
        self.model = QFileSystemModel()
        self.model.setRootPath((QDir.rootPath()))
        self.tree_view = QTreeView()
        self.tree_view.setModel(self.model)
        self.tree_view.setRootIndex(self.model.index(path))
        self.tree_view.setSortingEnabled(True)
        self.setWidget(self.tree_view)

    # def onClicked(self, index):
    #     self.sender().model().filePath(index)
