from PySide2.QtWidgets import QToolBar, QAction
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QFileDialog
import pandas as pd
import os


class ToolBar(QToolBar):
    def __init__(self, title="", parent=None):
        super().__init__(title, parent)
        self.button_add = QAction(
            QIcon("././resources/icons/blue-document.png"), "Add To Table", self
        )
        # self.button_remove = QAction("Remove", self)
        # self.button_change = QAction("Change", self)

        self.add_action()

        # self.tool_bar.button_open.triggered.connect(self.OpenFile)

    def add_action(self):
        self.addAction(self.button_add)
        # self.addAction(self.button_remove)
        # self.addAction(self.button_change)

    # def OpenFile(self):
    #     path = QFileDialog.getOpenFileName(
    #         self, "Open CSV", os.getenv(""), "CSV(*.csv)"
    #     )
    #     self.all_data = pd.read_csv(path[0])
    #     self.header = self.all_data.keys()
    #     self.list_data = self.all_data.values()
