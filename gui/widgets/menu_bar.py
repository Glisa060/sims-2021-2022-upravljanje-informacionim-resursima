from PySide2.QtWidgets import QMenuBar, QMenu, QAction, QFileDialog
from PySide2.QtGui import QIcon
from tr import tr
from gui.widgets.file_dock_widget import FileDockWidget


class MenuBar(QMenuBar):
    def __init__(self, parent=None):
        # poziv super inicijalizatora
        super().__init__(parent)

        # kreiranje osnovnih menija
        self.file_menu = QMenu("File", self)
        self.edit_menu = QMenu("Edit", self)
        self.help_menu = QMenu("Help", self)

        self

        self.open_action = QAction(
            QIcon("././resources/icons/blue-document.png"), "Open Folder", self
        )
        self.open_action.setShortcut("Ctrl+O")

        self.connect_to_database = QAction(
            QIcon("././resources/icons/database-sql.png"),
            "Connect to Database",
            self,
        )
        self.connect_to_database.setShortcut("Ctrl+D")

        # povezivanje menija
        self._populate_menues()
        self._add_buttons()

        self.open_action.triggered.connect(self._open_folder)

    def _populate_menues(self):
        self.addMenu(self.file_menu)
        self.addMenu(self.edit_menu)
        self.addMenu(self.help_menu)

    def _add_buttons(self):
        self.file_menu.addAction(self.open_action)
        self.file_menu.addAction(self.connect_to_database)

    def _open_folder(self):
        self.dialog = QFileDialog()
        self.file_path = self.dialog.getExistingDirectory(
            None, "Select folder:", "C:\\", QFileDialog.ShowDirsOnly
        )
        print(self.file_path)

        # self.dialog.setFileMode(QFileDialog.ExistingFiles)
        # self.dialog.setViewMode(QFileDialog.Detail)
        # self.dialog.setNameFilter("CSV Files (*.csv)")
        # self.file = QFileDialog.getOpenFileName(None, "Files", "", "CSV files (*.csv)")
