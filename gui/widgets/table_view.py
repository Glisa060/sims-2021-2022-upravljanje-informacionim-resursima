from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableView, QAbstractItemView
from PySide2.QtGui import QFont
from PySide2.QtCore import QSortFilterProxyModel
import sys

sys.path.append(
    "C:\\Users\\Milan\\Documents\\sims-2021-2022-upravljanje-informacionim-resursima"
)
from model.table_model import MyTableModel


class TableView(QWidget):
    def __init__(self, data_list, header):
        QWidget.__init__(self)
        self.setGeometry(300, 200, 570, 450)
        self.table_model = MyTableModel(self, data_list, header)
        self.table_view = QTableView()
        # self.table_view.setSelectionMode(QAbstractItemView.SingleSelection)
        # self.table_view.setSelectionMode(QAbstractItemView.ContiguousSelection)
        self.table_view.setEditTriggers(
            QAbstractItemView.AnyKeyPressed
            | QAbstractItemView.SelectedClicked
            | QAbstractItemView.DoubleClicked
        )
        self.table_view.horizontalHeader().sectionsMovable()
        # self.table_view.setSelectionBehavior(QAbstractItemView.SelectRows)
        # self.table_view.clicked.connect(self.showSelection())
        # self.table_view.clicked.connect(self.selectRow)
        self.table_view.setModel(self.table_model)
        self.table_view.setFont(QFont("Helvetica [Cronyx]", 12))
        self.table_view.resizeColumnsToContents()
        self.table_view.setSortingEnabled(True)
        layout = QVBoxLayout(self)
        layout.addWidget(self.table_view)
        self.setLayout(layout)

    # def update_model(self, data_list, header):
    #     self.table_model2 = MyTableModel(self, data_list, header)
    #     self.table_view.setModel(self.table_model2)
    #     self.table_view.update()

    # def showSelection(self, item):
    #     cellContent = item.data()
    #     # print(cellContent)  # test
    #     sf = "You clicked on {}".format(cellContent)
    #     # display in title bar for convenience
    #     self.setWindowTitle(sf)

    # def selectRow(self, index):
    #     # print("current row is %d", index.row())
    #     pass
