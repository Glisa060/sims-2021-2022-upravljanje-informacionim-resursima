from msilib.schema import File
from PySide2.QtWidgets import QMainWindow, QLabel, QFileDialog
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt

# lokalno definisane klase
from gui.widgets.menu_bar import MenuBar
from gui.widgets.tool_bar import ToolBar
from gui.widgets.status_bar import StatusBar
from gui.widgets.table_view import TableView
from gui.widgets.file_dock_widget import FileDockWidget
import pandas as pd
import os


class MainWindow(QMainWindow):
    def __init__(self, parent=None, user=None):
        # poziv super inicijalizaotra (QMainWindow)
        super().__init__(parent)
        # Osnovna podesavanja glavnog prozora
        self.setWindowTitle("Rukovalac informacionim resursima")
        self.setWindowIcon(QIcon("resources/icons/blue-document.png"))
        self.resize(1000, 650)
        self.all_data = []

        # inicijalizacija osnovnih elemenata GUI-ja
        self.tool_bar = ToolBar(self)
        self.status_bar = StatusBar(self)
        self.file_dock_widget = FileDockWidget("File Explorer", self)

        file = pd.read_csv("./data/users.csv").to_dict()
        list_data = file.values()
        header = file.keys()
        self.table_view = TableView(list_data, header)

        # uvezivanje elemenata GUI-ja
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.table_view)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.file_dock_widget, Qt.Vertical)

        # self.tool_bar.button_open.triggered.connect(self.OpenFile)

        # sacuvavanje prijavljenog korisnika iz dijaloga
        self.user = user
        self.status_bar.addWidget(
            QLabel("Ulogovani korisnik: " + self.user.user_id.upper())
        )
