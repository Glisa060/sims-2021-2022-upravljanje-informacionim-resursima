from csv import DictReader

from gui.main_window import MainWindow
import mysql.connector as mc
from tkinter import messagebox


class LoginController:
    def __init__(self):
        pass

    def login(self, model):
        # preuzmemo podatke sa forme za prijavu
        # ucitamo datoteku da proverimo da li postoji takav kao iz forme
        # FIXME: ovaj deo obrade datoteke (provere korisnika) izvuce u
        # drugi python file koji je file handler
        # FIXME: kasnije ce se podaci ucitavati iz baze podataka a ne datoteke

        mydb = mc.connect(
            host="localhost", user="root", password="root", database="login"
        )

        mycursor = mydb.cursor()
        mycursor.execute(
            "SELECT user_name from users where user_name  like '"
            + model.user_id
            + "'and user_pass like '"
            + model.user_password
            + "'"
        )
        result = mycursor.fetchone()

        if result == None:
            messagebox.showerror("Greska", "Pogresno ste uneli Username ili Password")

        else:
            return True
        # TODO: ako je prijava uspesna pozvati sopstvenu metodu start_app

    def password_change(self, new_password):
        ...

    def quit(self):
        ...

    def start_app(self, model=None):
        main_window = MainWindow()
        main_window.show()
